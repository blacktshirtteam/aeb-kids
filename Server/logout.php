<?php

require __DIR__ . '/vendor/autoload.php';

$db = new \PDO('mysql:dbname=cl221403_aeb-kids;host=localhost', 'cl221403_aeb', '123aeb.');

$auth = new \Delight\Auth\Auth($db);

$auth->logOut();

header('Location: index.php');

?>