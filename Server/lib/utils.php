<?php
function fromDB($str)
{
    return iconv("utf-8", "windows-1251", $str);
}

function toDB($str)
{
    return iconv("windows-1251", "utf-8", $str);
}

function mysqli2($host, $login, $pass, $db) {
    $conn = new mysqli($host, $login, $pass, $db);
    $conn->query("SET NAMES 'utf8'");
    $conn->query("SET CHARACTER SET 'utf8'");
    $conn->query("SET SESSION collation_connection = 'utf8_general_ci'");
    return $conn;
}

function mysqli3() {
    return mysqli2("localhost", "cl221403_aeb", "123aeb.", "cl221403_aeb-kids");
}

function exec_file($filename){
    ob_start();
    include $filename;
    $output = ob_get_contents();
    ob_end_clean();
    return $output;
}

function exec_text($text){
    ob_start();
    eval('?>'.$text.'<?php;');
    $output = ob_get_contents();
    ob_end_clean();
    return str_replace('<?php;','',$output);
}

function toFloat($str)
{
    return str_replace('.', ',', $str);
}

function toFloat2($str)
{
    return toFloat(round($str, 2));
}

function toMoney($str)
{
    return number_format($str, 2, ',', ' ');
}

function toMonth($m) {
    switch ($m) {
        case 1:
            return "Январь";
            break;
        case 2:
            return "Февраль";
            break;
        case 3:
            return "Март";
            break;
        case 4:
            return "Апрель";
            break;
        case 5:
            return "Май";
            break;
        case 6:
            return "Июнь";
            break;
        case 7:
            return "Июль";
            break;
        case 8:
            return "Август";
            break;
        case 9:
            return "Сентябрь";
            break;
        case 10:
            return "Октябрь";
            break;
        case 11:
            return "Ноябрь";
            break;
        case 12:
            return "Декабрь";
            break;
    }
}

function get_int_param($param)
{
    if (isset($_GET[$param])) return $_GET[$param]; else return 'null';
}

function get_str_param($param)
{
    if (isset($_GET[$param])) return '\''.$_GET[$param].'\''; else return 'null';
}
?>