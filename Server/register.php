<!doctype html>
<html lang="ru">
<head>
	<?php include 'head.inc' ?>
</head>
<body class="reg_bg">

<?php

require __DIR__ . '/vendor/autoload.php';
require_once 'lib/utils.php';

$db = new \PDO('mysql:dbname=cl221403_aeb-kids;host=localhost', 'cl221403_aeb', '123aeb.');

$auth = new \Delight\Auth\Auth($db);

if (isset($_POST['email'])) {
try {
    $userId = $auth->register($_POST['email'] . '@aeb.ru', $_POST['password_main'], $_POST['email'], function ($selector, $token) {
        global $auth;
        $auth->confirmEmail($selector, $token);
    });
    $conn = mysqli3();
    $conn->query('call parent_add (\'' . $userId . '\', \'' . $_POST['firstname'] . '\', \'' . $_POST['lastname'] . '\', NULL , \'' . $_POST['phone'] . '\')');
    $conn->close();
    header('Location: index.php');
}
catch (\Delight\Auth\InvalidEmailException $e) {
    die('Invalid email address');
}
catch (\Delight\Auth\InvalidPasswordException $e) {
    die('Invalid password');
}
catch (\Delight\Auth\UserAlreadyExistsException $e) {
    die('User already exists');
}
catch (\Delight\Auth\TooManyRequestsException $e) {
    die('Too many requests');
}
}
?>

<div class="card content mx-auto bg-white reg_form mt-5">
    <div class="card-header font-weight-bold text-center">Регистрация</div>
	<div class="card-body">
		<form method="post">
            <div class="form-group">
                <label for="email" class="font-weight-bold">Логин</label>
                <input type="text" class="form-control" name="email" id="email" required>
            </div>
			<div class="form-group">
                <label for="password_main" class="font-weight-bold">Пароль</label>
                <input type="password" class="form-control" name="password_main" id="password_main" required>
            </div>
            <div class="form-group">
                <label for="firstname" class="font-weight-bold">Имя</label>
                <input type="text" class="form-control" name="firstname" id="firstname" required>
            </div>
            <div class="form-group">
                <label for="lastname" class="font-weight-bold">Фамилия</label>
                <input type="text" class="form-control" name="lastname" id="lastname" required>
            </div>
            <div class="form-group">
                <label for="phone" class="font-weight-bold">Телефон</label>
                <input type="text" class="form-control" name="phone" id="phone" required>
            </div>
            <div class="form-check">
                <input type="checkbox" class="form-check-input" name="agreement" id="agreement" required>
                <label for="agreement" class="form-check-label">Я принимаю условия Пользовательского соглашения</label>
            </div>
			<button type="submit" class="btn btn-info form-control mt-4">Зарегистрироваться</button>
		</form>
	</div>
</div>

<div class="card content mx-auto bg-white reg_form mt-5">
    <div class="card-body text-center">
        Уже зарегистрированы? <a href="/index.php">Войдите</a>
    </div>
</div>

</body>
</html>
                    
                    

 
