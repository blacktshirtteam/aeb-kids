<?php

require_once '../lib/utils.php';

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0", false);
header("Cache-Control: max-age=0", false);
header("Pragma: no-cache");
header('Content-type: application/json; charset=utf-8');

if (isset($_POST['id'])) {

    $id = 0;
    $description = '';
    $secondname = '';
    $birthday = '';
    $vcardnumber = '';
    $vcardlimit = 0;
    $vcardbalance = 0;
    $events = [];

    $conn = mysqli3();
    $result = $conn->query('call events(' . $_POST['id'] . ')');
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $id = $row['id'];
        $description = $row['description'];
    }
    $result->close();
    $conn->close();

    if ($id != 0) {
        $events = array(
            'result' => 0,
            'id' => $id,
            'description' => $description
        );
    } else {
        $events = array('result' => 404);
    }

    echo json_encode($events);
}

