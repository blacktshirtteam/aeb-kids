<?php

require __DIR__ . '../vendor/autoload.php';

$db = new \PDO('mysql:dbname=cl221403_aeb-kids;host=localhost', 'cl221403_aeb', '123aeb.');

$auth = new \Delight\Auth\Auth($db);

if (isset($_POST['login'])) {
    try {
        $userId = $auth->register($_POST['login'] . '@aeb.ru', $_POST['password'], $_POST['login'], function ($selector, $token) {
            echo 'Send ' . $selector . ' and ' . $token . ' to the user (e.g. via email)';
        });

        echo 'We have signed up a new user with the ID ' . $userId;
    }
    catch (\Delight\Auth\InvalidEmailException $e) {
        die('Invalid email address');
    }
    catch (\Delight\Auth\InvalidPasswordException $e) {
        die('Invalid password');
    }
    catch (\Delight\Auth\UserAlreadyExistsException $e) {
        die('User already exists');
    }
    catch (\Delight\Auth\TooManyRequestsException $e) {
        die('Too many requests');
    }
}