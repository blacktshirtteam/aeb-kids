<?php

require_once '../lib/utils.php';

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0", false);
header("Cache-Control: max-age=0", false);
header("Pragma: no-cache");
header('Content-type: application/json; charset=utf-8');

if (isset($_POST['id'])) {

    $id = 0;
    $tip = '';
    $tips = [];

    $conn = mysqli3();
    $result = $conn->query('call tips()');
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $id = $row['id'];
        $tip = $row['value'];
    }
    $result->close();
    $conn->close();

    if ($id != 0) {
        $tips =array(
            'result' => 0,
            'id' => $id,
            'tip' => $tip
        );
    } else {
        $tips = array('result' => 404);
    }

    echo json_encode($tips);
}