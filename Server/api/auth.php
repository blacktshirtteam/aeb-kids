<?php

require_once '../lib/utils.php';

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0", false);
header("Cache-Control: max-age=0", false);
header("Pragma: no-cache");
header('Content-type: application/json; charset=utf-8');

if (isset($_POST['phone'])) {

    $id = 0;
    $firstname = '';
    $secondname = '';
    $birthday = '';
    $vcardnumber = '';
    $vcardlimit = 0;
    $vcardbalance = 0;
    $kids = [];

    $conn = mysqli3();
    $result = $conn->query('call auth(\'' . $_POST['phone'] . '\')');
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $id = $row['id'];
        $firstname = $row['firstname'];
        $lastname = $row['lastname'];
        $birthday = $row['birthday'];
        $vcardnumber = $row['number'];
        $vcardlimit = $row['limit'];
        $vcardbalance = $row['balance'];
    }
    $result->close();
    $conn->close();

    if ($id != 0) {
        $kids =array(
            'result' => 0,
            'id' => $id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'birthday' => $birthday,
            'vcardnumber' => $vcardnumber,
            'vcardlimit' => $vcardlimit,
            'vcardbalance' => $vcardbalance
        );
    } else {
        $kids = array('result' => 404);
    }

    echo json_encode($kids);
}

