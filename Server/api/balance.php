<?php

require_once '../lib/utils.php';

header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-cache, must-revalidate");
header("Cache-Control: post-check=0,pre-check=0", false);
header("Cache-Control: max-age=0", false);
header("Pragma: no-cache");
header('Content-type: application/json; charset=utf-8');

if (isset($_POST['id'])) {

    $id = 0;
    $firstname = '';
    $secondname = '';
    $purchases = 0;
    $limit = 0;
    $balance = 0;
    $kid = [];

    $conn = mysqli3();
    $result = $conn->query('call kid(\'' . $_POST['id'] . '\')');
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
        $id = $row['id'];
        $firstname = $row['firstname'];
        $lastname = $row['lastname'];
        $purchases = $row['purchases'];
        $limit = $row['limit'];
        $balance = $row['balance'];
    }
    $result->close();
    $conn->close();

    if ($id != 0) {
        $kid =array(
            'result' => 0,
            'id' => $id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'purchases' => $purchases,
            'limit' => $limit,
            'balance' => $balance
        );
    } else {
        $kid = array('result' => 404);
    }

    echo json_encode($kid);
}

