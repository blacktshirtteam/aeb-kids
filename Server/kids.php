<!doctype html>
<html lang="ru">
<head>
    <?php include 'head.inc' ?>
</head>
<body>

<?php include 'header.inc' ?>

<div class="main-content mx-auto container-fluid">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb my-md-5 my-2">
            <li class="breadcrumb-item active" aria-current="page"><h4>Мои дети</h4></li>
        </ol>
    </nav>

    <?php
    require_once 'lib/utils.php';

    $conn = mysqli3();
    $result = $conn->query('call kids(\'' . $auth->getUserId() . '\')');
    while ($row = $result->fetch_array(MYSQLI_ASSOC)) {
    ?>

    <div class="row justify-content-center my-md-5 my-2">
        <div class="col-12">
            <div class="card mx-auto">
                <div class="card-header"><?=$row['firstname'].' '.$row['lastname']?></div>
                <div class="card-body">
                    <div class="flex-row mb-1">
                        <div class="flex-column">Дневной лимит: <?=$row['limit']?> руб.</div>
                        <div class="flex-column">Остаток: <?=$row['limit']-$row['purchases']?> руб.</div>
                        <hr class="divider">
                        <div class="flex-column">Баланс карты: <?=$row['balance']-$row['purchases']?> руб.</div>
                    </div>
                    <hr class="divider">
                    <div class="row mt-1">
                        <div class="col-md-6 mb-1"><a href="/balance.php?id=<?=$row['id']?>" class="btn btn-outline-success w-100">Пополнить баланс</a></div>
                        <div class="col-md-6 mb-1"><a href="/limit.php?id=<?=$row['id']?>" class="btn btn-outline-success w-100">Повысить лимит</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    }
    $result->close();
    $conn->close();
    ?>

</div>

<?php include 'footer.inc' ?>

</body>
</html>
