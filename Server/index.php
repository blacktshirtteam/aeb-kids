<!doctype html>
<html lang="ru">
<head>
    <?php include 'head.inc' ?>
</head>
<body class="auth_bg">

<?php

require __DIR__ . '/vendor/autoload.php';

$db = new \PDO('mysql:dbname=cl221403_aeb-kids;host=localhost', 'cl221403_aeb', '123aeb.');

$auth = new \Delight\Auth\Auth($db);

if ($auth->isLoggedIn()) header('Location: kids.php');

if (isset($_POST['email'])) {
    try {
        $auth->login($_POST['email'] . '@aeb.ru', $_POST['password'], (int) (60 * 60 * 24 * 365.25));

        header('Location: kids.php');
    }
    catch (\Delight\Auth\InvalidEmailException $e) {
        die('Wrong email address');
    }
    catch (\Delight\Auth\InvalidPasswordException $e) {
        die('Wrong password');
    }
    catch (\Delight\Auth\EmailNotVerifiedException $e) {
        die('Email not verified');
    }
    catch (\Delight\Auth\TooManyRequestsException $e) {
        die('Too many requests');
    }
}
?>

<div class="card content mx-auto bg-white reg_form mt-5">
    <div class="card-header font-weight-bold text-center">Вход</div>
    <div class="card-body">
        <form method="post">
            <div class="form-group">
                <label for="email" class="font-weight-bold">E-mail</label>
                <input type="text" class="form-control" name="email" id="email" required>
            </div>
            <div class="form-group">
                <label for="password" class="font-weight-bold">Пароль</label>
                <input type="password" class="form-control" name="password" id="password" required>
            </div>
            <button type="submit" class="btn btn-info form-control mt-4">Войти</button>
        </form>
    </div>
</div>

<div class="card content mx-auto bg-white reg_form mt-5">
    <div class="card-body text-center">
        Ещё нет аккаунта? <a href="/register.php">Зарегистрируйтесь</a>
    </div>
</div>

</body>
</html>




