<?php

require __DIR__ . '/vendor/autoload.php';

$db = new \PDO('mysql:dbname=cl221403_aeb-kids;host=localhost', 'cl221403_aeb', '123aeb.');

$auth = new \Delight\Auth\Auth($db);

if (!$auth->isLoggedIn()) header('Location: index.php');

?>

<nav class="navbar navbar-expand-lg navbar-light bg-dark sticky-top">
    <a href="/index.php" class="navbar-brand text-light font-weight-bold">АЭБ-Kids Родитель</a>
    <button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li><a href="/kids.php" class="nav-link text-light">Мои дети</a></li>
            <li><a href="/cards.php" class="nav-link text-light">Карты</a></li>
            <li><a href="/cards.php" class="nav-link text-light">История покупок</a></li>
        </ul>

        <ul class="navbar-nav navbar-right">
            <div class="dropdown-divider"></div>
            <li><a href="logout.php" class="nav-link btn bg-secondary text-light">Выход</a></li>
        </ul>
    </div>
</nav>