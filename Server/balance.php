<!doctype html>
<html lang="ru">
<head>
    <?php include 'head.inc' ?>
</head>
<body>

<?php include 'header.inc' ?>

<div class="main-content mx-auto container-fluid">
    <?php
    if (isset($_GET['id'])) {
        ?>

        <nav aria-label="breadcrumb">
            <ol class="breadcrumb my-md-5 my-2">
                <li class="breadcrumb-item active" aria-current="page"><h4>Пополнение баланса</h4></li>
            </ol>
        </nav>

        <div class="row justify-content-center my-md-5 my-2">
            <div class="col-12">
                <div class="card mx-auto">
                    <div class="card-body">
                        <div class="flex-row mb-1">
                            <div class="flex-column">Дневной лимит: <?=$row['limit']?> руб.</div>
                            <div class="flex-column">Дневной баланс: <?=$row['limit']-$row['purshases']?> руб.</div>
                            <hr class="divider">
                            <div class="flex-column">Баланс: <?=$row['balance']-$row['purshases']?> руб.</div>
                        </div>
                        <hr class="divider">
                        <div class="row mt-1">
                            <div class="col"><a href="/balance?id=<?=$row['id'].'&card=1&amount=100000'?>" class="btn btn-success w-100">Пополнить</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
    } else {
    ?>
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb my-md-5 my-2 bg-danger">
            <li class="breadcrumb-item active text-white" aria-current="page"><h4>Выберите ребенка и повторите попытку</h4></li>
        </ol>
    </nav>
    <?php
    }
    ?>
</div>

<?php include 'footer.inc' ?>

</body>
</html>
