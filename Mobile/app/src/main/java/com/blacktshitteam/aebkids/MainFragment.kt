package com.blacktshitteam.aebkids


import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.blacktshitteam.aebkids.behavior.AsyncPOSTRequest
import org.json.JSONObject

class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onStart() {
        super.onStart()

        val a = AsyncPOSTRequest()
        val b = AsyncPOSTRequest()
        val id = activity!!.getSharedPreferences("loginData", Context.MODE_PRIVATE).getInt("userID", -1)
        a.execute(
            "http://${getString(R.string.host_name)}/api/balance.php",
            "id=$id"
        )
        val resp = JSONObject(a.get())
        if (resp.optInt("result") == 0) {
            activity!!.findViewById<TextView>(R.id.balance_text).setText("${(resp.optDouble("balance") - resp.optDouble("purchases"))} \u20BD (${resp.optDouble("limit") - resp.optDouble("purchases")} \u20BD)")
        }

        b.execute(
            "http://${getString(R.string.host_name)}/api/tips.php",
            "id=$id"
        )
        val r = b.get()
        val resp1 = JSONObject(r)
        if (resp1.optInt("result") == 0) {
            activity!!.findViewById<TextView>(R.id.tip_text).setText(resp1.optString("tip"))
        }
    }



}
