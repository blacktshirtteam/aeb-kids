package com.blacktshitteam.aebkids

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.util.Patterns
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText

import org.json.JSONObject

import com.blacktshitteam.aebkids.behavior.AsyncPOSTRequest
import kotlinx.android.synthetic.main.activity_auth.*
import org.json.JSONArray

class AuthActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_auth)

        loginButton.setOnClickListener {
            val phoneNumber = phoneNumber.text.toString()
            if (phoneNumber.isNotEmpty() && Patterns.PHONE.matcher(phoneNumber).matches()) {
                var a = AsyncPOSTRequest()
                a.execute(
                    "http://${getString(R.string.host_name)}/api/auth.php",
                    "phone=$phoneNumber" // 79142367593
                )
                val r = a.get()
                val resp = JSONObject(r)
                if (resp.optInt("result") == 0) {
                    var fos = openFileOutput("phone_number.txt", Context.MODE_PRIVATE)
                    fos.write(phoneNumber.toByteArray(charset("UTF-8")))
                    fos.close()

                    var prefsEditor = getSharedPreferences("loginData", Context.MODE_PRIVATE).edit()
                    prefsEditor.putInt("userID", resp.optInt("id"))
                    prefsEditor.putString("userFirstName", resp.optString("firstname"))
                    prefsEditor.putString("userLastName", resp.optString("lastname"))
                    prefsEditor.putString("userBirthday", resp.optString("birthday"))
                    prefsEditor.putString("userVCardNumber", resp.optString("vcardnumber"))
                    prefsEditor.putString("userVCardLimit", resp.optString("vcardlimit"))
                    prefsEditor.putString("userVCardBalance", resp.optString("vcardbalance"))

                    intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
            }
        }
    }
}
