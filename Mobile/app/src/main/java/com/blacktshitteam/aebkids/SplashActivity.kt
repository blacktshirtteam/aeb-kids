package com.blacktshitteam.aebkids

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import org.json.JSONObject

import com.blacktshitteam.aebkids.behavior.AsyncPOSTRequest
import org.json.JSONArray


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent : Intent

        val filePhone = baseContext.getFileStreamPath("phone_number.txt")
        if (filePhone.exists()) {
            var a = AsyncPOSTRequest()
            a.execute(
                "http://" + getString(R.string.host_name) + "/api/auth.php",
                "phone=" + filePhone.readText() // 79142367593
            ) // да, это полная жесть, но пусть пока будет так
            val resp = JSONObject(a.get())
            if (resp.optInt("return") == 0) {
                var prefsEditor = getSharedPreferences("loginData", Context.MODE_PRIVATE).edit()
                prefsEditor.putInt("userID", resp.optInt("id"))
                prefsEditor.putString("userFirstName", resp.optString("firstname"))
                prefsEditor.putString("userLastName", resp.optString("lastname"))
                prefsEditor.putString("userBirthday", resp.optString("birthday"))
                prefsEditor.putString("userVCardNumber", resp.optString("vcardnumber"))
                prefsEditor.putString("userVCardLimit", resp.optString("vcardlimit"))
                prefsEditor.putString("userVCardBalance", resp.optString("vcardbalance"))
                prefsEditor.apply()

                intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
                finish()
                return
            }
        }

        intent = Intent(this, AuthActivity::class.java)
        startActivity(intent)
        finish()
    }
}