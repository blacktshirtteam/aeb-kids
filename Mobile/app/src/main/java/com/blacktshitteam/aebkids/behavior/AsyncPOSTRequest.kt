package com.blacktshitteam.aebkids.behavior

import android.os.AsyncTask
import java.net.HttpURLConnection
import java.net.URL
import java.io.*

import com.blacktshitteam.aebkids.R
import com.blacktshitteam.aebkids.App
import com.blacktshitteam.aebkids.SplashActivity


class AsyncPOSTRequest : AsyncTask<String, String, String>() {

    override fun doInBackground(vararg params: String?): String {
        val urlString = params[0]
        val data = params[1] //data to post
        var outStream: OutputStream? = null
        var inStream: InputStream? = null

        try {
            val connection = URL(urlString).openConnection() as HttpURLConnection
            connection.requestMethod = "POST"
            connection.doInput = true
            connection.doOutput = true
            outStream = BufferedOutputStream(connection.outputStream)

            val writer = BufferedWriter(OutputStreamWriter(outStream, "UTF-8"))
            writer.write(data)
            writer.flush()
            writer.close()
            outStream.close()

            connection.connect()

            inStream = BufferedInputStream(connection.inputStream)
            val reader = BufferedReader(InputStreamReader(inStream, "UTF-8"))
            val ret = reader.readText()
            inStream.close()

            return ret

        } catch (e: Exception) {
            println(e.message)
        }

        return ""
    }
}