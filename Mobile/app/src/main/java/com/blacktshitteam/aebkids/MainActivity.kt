package com.blacktshitteam.aebkids

import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.Window
import kotlinx.android.synthetic.main.activity_main.*
import androidx.navigation.Navigation


class MainActivity : FragmentActivity () {

    override fun onCreate(savedInstanceState: Bundle?) {
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottom_navigation.setOnNavigationItemSelectedListener {
            if (bottom_navigation.selectedItemId == it.itemId)
                return@setOnNavigationItemSelectedListener false
            when (it.itemId) {
                R.id.nav_connect -> {
                    Navigation.findNavController(findViewById(R.id.container)).navigate(R.id.toConnect)
                    true
                }
                R.id.nav_main -> {
                    Navigation.findNavController(findViewById(R.id.container)).navigate(R.id.toMain)
                    true
                }
                R.id.nav_stat -> {
                    Navigation.findNavController(findViewById(R.id.container)).navigate(R.id.toStats)
                    true
                }
                else -> false
            }
        }
    }
}
